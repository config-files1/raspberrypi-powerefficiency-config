# Headless-RaspberryPi-Efficiency-config
Config for a headless raspberry pi that addresses preformance while maintaining power efficiency

## Copy from here
/boot/config.txt

```
# See /boot/overlays/README for all available option

# Enable DRM VC4 V3D driver
dtoverlay=vc4-fkms-v3d

[all]
dtoverlay=disable-wifi
dtoverlay=disable-bt

[all]
gpu_mem=16
```

~/.bashrc

```
#
# ~/.bashrc
#

# Disable HDMI output
/usr/bin/tvservice -o
```

Resources:
* https://blues.io/blog/tips-tricks-optimizing-raspberry-pi-power/

